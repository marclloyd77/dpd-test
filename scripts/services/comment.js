'use strict';
angular.module('dpdTestApp')
    .factory('comment', function ( $resource ) {
        return $resource(
            'http://jsonplaceholder.typicode.com/posts/:id/comments', { id: '@id' }, {
            }
        );
    });