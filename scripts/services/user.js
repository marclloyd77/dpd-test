'use strict';
angular.module('dpdTestApp')
    .factory('user', function ( $resource ) {
        return $resource(
            'http://jsonplaceholder.typicode.com/users/:id', { id: '@id' }, {
            }
        );
    });