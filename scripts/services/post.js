'use strict';
angular.module('dpdTestApp')
    .factory('post', function ( $resource ) {
        return $resource(
            'http://jsonplaceholder.typicode.com/posts/:id', { id: '@id' }, {
            }
        );
    })