'use strict';
angular.module('dpdTestApp')
    .controller('UserCtrl', function ($scope, user){
        user.query().$promise.then(function(data){
            $scope.users = data;
        })
    });