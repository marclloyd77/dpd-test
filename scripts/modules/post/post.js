'use strict';
angular.module('dpdTestApp')
    .controller('PostCtrl', function ($scope, user, post, $routeParams){
        user.get({id:$routeParams.id}).$promise.then(function(data){
            $scope.user = data;
        });
        post.query({userId:$routeParams.id}).$promise.then(function(data){
            $scope.posts = data;
        });
    });